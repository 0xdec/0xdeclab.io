---
title: "About"
image: "profile.jpg"
weight: 8
---

I'm **Jordi Pakey-Rodriguez** -- a hardware engineer living and working in San Francisco. I often go by **Jordi Orlando** online.

### Résumé

[Latest](home/resume_2021-06-16.pdf)
|
[May 2020](home/resume_2020-05-20.pdf)
