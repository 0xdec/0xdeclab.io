---
title: "1BitSquared"
weight: 2
project_timeframe: 2020-2021
---

I worked with [1BitSquared](https://1bitsquared.com) to design several products, such as [iCEBreaker Bitsy](https://docs.icebreaker-fpga.org/hardware/bitsy).
