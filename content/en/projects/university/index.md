---
title: "University"
weight: 3
resources:
    - src: downbeat.png
      title: Downbeat
      params:
          weight: 1
    - src: bcan.png
      title: BCAN
      params:
          weight: 2
    - src: clarinet.png
      title: Self-Playing Clarinet
      params:
          weight: 3
project_timeframe: 2013-2018
---

During my time in the [Marching Illini](http://marchingillini.com) I built an electronic drill app called [Downbeat](https://downbeat.app).

My undergrad senior design project, [BCAN](https://bcan.jordi.page), aimed to develop autonomy for complex model train systems.

I also built a [self-playing clarinet](https://clarinet.jordi.page) for my Advanced Digital Projects Laboratory.
